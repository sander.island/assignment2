# Assignment 2

### Participants
* Martin Mikkelsen
* Isar Buzza
* Sander Island

## Description
This project is a group project for assignment 2 in Noroff's Java Fullstack course. In this project, we have been provided a database, and our task is to write SQL statements/queries to extract desired data.
### The project is divided in to two parts:
#### Superheroes
You are to create several scripts which can be run to create a database, setup some tables in the database, add
relationships to the tables, and then populate the tables with data.<br />
The database and its theme are surrounding superheroes. The database can be called SuperheroesDb.<br />
REQUIREMENT: Create a database in PgAdmin called Superheroes. All subsequent scripts are to be run from there.

#### Chinook
Some hotshot media mogul has heard of your newly acquired skills in Java. They have contracted you and a friend to
stride on the edge of copyright glory and start re-making iTunes, but under a different name. They have spoken to
lawyers and are certain a working prototype should not cause any problems and ensured that you will be safe. The
lawyer they use is the same that Epic has been using, so they are familiar with Apple.<br /><br />
The second part of the assignment deals with manipulating SQL data in Spring using a the JDBC with the PostgreSQL
driver. For this part of the assignment, you are given a database to work with. It is called Chinook. You need to first
create the database in PgAdmin, then open a query tool and drag the script in and run it. It should generate all the tables
and populate it.<br /><br />
Chinook models the iTunes database of customers purchasing songs. You are to create a Spring Boot application, include
the correct driver, and create a repository pattern to interact with the database.

### Tools
* IntelliJ has been used for coding
* pgAdmin has been used for handling the database
* Postgres SQL


## Where to find what
* The Superheroes SQL files are located in **Assignment2/sql**
* Chinook related classes are located in **Assignment2/src**

### Git usage
We had our own branches where each contributer worked on their given tasks.

#### Note
We are not using the gitlab-ci.yml file because we had to provide credit card information and we find that unnecessary for this case.
