package assignmnet2.assignment2.models;

public record Spender(int id, String firstName, String lastName, double total) {
}
