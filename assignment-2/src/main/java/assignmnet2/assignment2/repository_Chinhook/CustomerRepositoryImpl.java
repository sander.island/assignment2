package assignmnet2.assignment2.repository_Chinhook;

import assignmnet2.assignment2.models.Country;
import assignmnet2.assignment2.models.Customer;
import assignmnet2.assignment2.models.Genre;
import assignmnet2.assignment2.models.Spender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository{
    private final String url;
    private final String username;
    private final String password;


    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * This method returns every row in the 'customer' table.
     * @return List of Customer objects
     */
    @Override
    public List<Customer> findall() {
        List<Customer> customers = new ArrayList<Customer>();
        try(Connection conn = DriverManager.getConnection(url, username,password)){
            String sql = "select * from customer";
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                customers.add(new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                ));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    /**
     * This method finds and returns a given customer by the 'customer_id' field.
     * @param id Customer_id for a given customer in the customer table.
     * @return Customer object with given customer_id.
     */
    @Override
    public Customer findById(Integer id) {
        try(Connection conn = DriverManager.getConnection(url, username,password)){
            String sql = "select * from customer where customer_id = ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if(result.next()){
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                return customer;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method searches the customer table for customers that contains/equals the 'name' parameter,
     * in both first name and last name.
     * @param name Full or partial name of the customer(s) you want to search for.
     * @return List of Customer objects that contains given name.
     */
    @Override
    public List<Customer> findByName(String name) {
        List<Customer> customers = new ArrayList<Customer>();
        try(Connection conn = DriverManager.getConnection(url, username,password)){
            String sql = "select * from customer where first_name ilike ? or last_name ilike ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + name + "%");
            statement.setString(2, "%" + name + "%");
            ResultSet result = statement.executeQuery();
            while(result.next()){
                customers.add(new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                ));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    /**
     * Inserts a new row *in the customer table.
     *
     * @param customer
     * The customer object is required to have the following values:
     * First name, Last name, Country, Postal Code, Phone number and email.
     *
     * @return
     * Returns result and a confirmation message.
     */
    @Override
    public int insert(Customer customer) {
        int result = 0;

        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            String sql = String.format("INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) " +
                    "VALUES (?, ?, ?, ?, ?, ?)");
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setString(3, customer.getCountry());
            statement.setString(4, customer.getPostalCode());
            statement.setString(5, customer.getPhoneNumber());
            statement.setString(6, customer.getEmail());

            result = statement.executeUpdate();
            System.out.println("Successfully created user " + customer.getFirstName());

            }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Updates an existing row in the customer table.
     * @param customer
     * The method takes the customer_id of the provided object and
     * updates the row with new values provided through setters of the object.
     * @return
     * Returns a confirmation message in the console.
     */
    @Override
    public int update(Customer customer) {
        int result = 0;

        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            String sql = String.format("UPDATE customer " +
                    "SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? " +
                    "WHERE customer_id = %s", customer.getId());
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setString(3, customer.getCountry());
            statement.setString(4, customer.getPostalCode());
            statement.setString(5, customer.getPhoneNumber());
            statement.setString(6, customer.getEmail());


            result = statement.executeUpdate();
            System.out.println("Successfully updated user " + customer.getId());

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * This method finds the country with the highest number of customers.
     * @return country
     */
    @Override
    public Country mostCustomersCountry() {
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            Country country = null;
            // Statement should be correct, but also show the count, and not only the country
            String sql = "SELECT country, COUNT(customer_id) AS num_of_customers FROM customer GROUP BY country ORDER BY COUNT (customer_id) DESC LIMIT 1;";
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                country = new Country(
                        result.getString("country"),
                        result.getInt("num_of_customers")
                );
            }
            return country;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    /**
     * This method finds the highest spender.
     */
    public Spender highestSpender() {
        Spender spender = null;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            String sql = "SELECT invoice.customer_id, customer.*, invoice.total " +
                    "FROM invoice " +
                    "INNER JOIN customer ON invoice.customer_id=customer.customer_id " +
                    "ORDER BY invoice.total DESC LIMIT 1;";
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                spender = new Spender(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getDouble("total")
                );
            }
            return spender;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return spender;
    }

    @Override
    /**
     * This method shows a given customer's most popular genre.
     * Known issue: will not show two genres in case of a tie.
     */
    public Genre mostPopularGenre(Integer id) {
        Genre genre = null;
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            String sql = "SELECT customer.*, genre.name AS genre_name, COUNT(customer.customer_id) " +
                    "FROM customer " +
                    "INNER JOIN invoice ON invoice.customer_id = customer.customer_id " +
                    "INNER JOIN invoice_line ON invoice_line.invoice_id = invoice.invoice_id " +
                    "INNER JOIN track ON track.track_id = invoice_line.track_id " +
                    "INNER JOIN genre ON genre.genre_id = track.genre_id " +
                    "WHERE customer.customer_id = ? " +
                    "GROUP BY customer.customer_id, genre.name " +
                    "ORDER BY COUNT (customer.customer_id) DESC LIMIT 1;";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                genre = new Genre(result.getString("genre_name"));
            }
            return genre;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genre;
    }

    /**
     * Returns a given amount of customer rows.
     * @param limit
     * Defines the number of rows to return.
     * @param offset
     * Defines the number of rows to offset the returned rows.
     * Offset 5 would start at customer_id 6.
     * @return
     * Returns a list of customer objects.
     */
    @Override
    public List<Customer> getLimited(Integer limit, Integer offset) {
        List<Customer> customerPage = new ArrayList<Customer>();
        try(Connection conn = DriverManager.getConnection(url, username,password)){
            String sql = String.format("SELECT * FROM customer LIMIT %s OFFSET %s",limit, offset);
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                Customer customer = new Customer();
                customer.setId(result.getInt("customer_id"));
                customer.setFirstName(result.getString("first_name"));
                customer.setLastName(result.getString("last_name"));
                customer.setCountry(result.getString("country"));
                customer.setPostalCode(result.getString("postal_code"));
                customer.setPhoneNumber(result.getString("phone"));
                customer.setEmail(result.getString("email"));
                customerPage.add(customer);

            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customerPage;
    }

    /**
     * This method tries to connect with the database and print the result.
     */
    @Override
    public void test() {
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            System.out.println("Connected to Postgres...");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
