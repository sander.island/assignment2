package assignmnet2.assignment2.repository_Chinhook;

import java.util.List;

public interface CRUDRepository <T, U>{
    List<T> findall();
    List<T> getLimited(U limit, U offset);
    T findById(U id);
    int insert(T object);
    int update(T object);
}
