package assignmnet2.assignment2.repository_Chinhook;

import assignmnet2.assignment2.models.Country;
import assignmnet2.assignment2.models.Customer;
import assignmnet2.assignment2.models.Genre;
import assignmnet2.assignment2.models.Spender;

import java.util.List;

public interface CustomerRepository extends CRUDRepository<Customer, Integer>{

    // Shows the Country with the most customers
    Country mostCustomersCountry();

    // Shows the highest spending customer.
    // Customer who is the highest spender (total in invoice table is the largest).
    Spender highestSpender();

    // Shows a given customer's most popular genre
    Genre mostPopularGenre(Integer id);

    public void test();
    public List<Customer> findByName(String name);
}
