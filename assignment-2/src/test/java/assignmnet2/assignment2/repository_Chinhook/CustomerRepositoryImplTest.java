package assignmnet2.assignment2.repository_Chinhook;

import assignmnet2.assignment2.models.Country;
import assignmnet2.assignment2.models.Customer;
import ch.qos.logback.core.util.COWArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CustomerRepositoryImplTest {
    CustomerRepositoryImpl customerRepository = new CustomerRepositoryImpl("jdbc:postgresql://localhost:5432/Chinhook", "postgres", "postgres");

    @Test
    void findall() {
        List<Customer> customers = customerRepository.findall();
        assertTrue(customers.size() > 0);
    }

    @Test
    void findById() {
        Customer customer = new Customer();
        assertEquals(Customer.class, customerRepository.findById(1).getClass());
    }

    @Test
    void findByName() {
        List<Customer> customers = new ArrayList<>();
        assertEquals(customers.getClass(), customerRepository.findByName("a").getClass());
    }

    @Test
    void insert() {
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }

    @Test
    void deleteById() {
    }

    @Test
    void mostCustomersCountry() {
        // Arrange
        String expectedCountry = "USA";
        int expectedCustomers = 13;

        // Act
        String actualCountry = customerRepository.mostCustomersCountry().country();
        int actualCustomers = customerRepository.mostCustomersCountry().customers();

        // Assert
        assertEquals(expectedCountry, actualCountry);
        assertEquals(expectedCustomers, actualCustomers);
    }

    @Test
    void highestSpender() {
        // Arrange
        int expectedCustomerID = 6;
        String expectedFirstName = "Helena";
        String expectedLastName = "Holý";

        // Act
        int actualCustomerID = customerRepository.highestSpender().id();
        String actualFirstName = customerRepository.highestSpender().firstName();
        String actualLastname = customerRepository.highestSpender().firstName();

        // Assert
        assertEquals(expectedCustomerID, actualCustomerID);
        assertEquals(expectedFirstName, actualFirstName);
        assertEquals(expectedLastName, actualLastname);
    }

    @Test
    void mostPopularGenre() {
        // Arrange
        String expectedGenreName = "Rock";

        // Act
        String actualGenreName = customerRepository.mostPopularGenre(8).name();

        // Assert
        assertEquals(expectedGenreName,actualGenreName);
    }

    @Test
    void getLimited() {
    }
}