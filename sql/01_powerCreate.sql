DROP TABLE IF EXISTS power;

CREATE TABLE power (
    power_id serial PRIMARY KEY,
    name varchar(45) NOT NULL,
    description varchar(45) NOT NULL
);