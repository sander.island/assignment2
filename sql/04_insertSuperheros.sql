INSERT INTO superhero (name, alias, origin) VALUES ('Bruce Wayne', 'Batman', 'Gotham City');
INSERT INTO superhero (name, alias, origin) VALUES ('Clark Kent', 'Superman', 'Krypton');
INSERT INTO superhero (name, alias, origin) VALUES ('Thor Odinson', 'Thor', 'Åsgard');
INSERT INTO superhero (name, alias, origin) VALUES ('Tony Stark', 'Ironman', 'USA');
INSERT INTO superhero (name, alias, origin) VALUES ('Peter Parker', 'Spider-Man', 'USA');