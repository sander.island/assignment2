DROP TABLE IF EXISTS superhero CASCADE;
CREATE TABLE superhero (
    id serial NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL,
    alias varchar(255) NOT NULL,
    origin varchar(255)
);