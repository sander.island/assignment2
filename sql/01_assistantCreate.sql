DROP TABLE IF EXISTS assistant;
CREATE TABLE assistant (
    id serial NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL
);

ALTER TABLE assistant ADD COLUMN superhero_id int REFERENCES superhero(id);