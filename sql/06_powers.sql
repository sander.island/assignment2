INSERT INTO power
(name, description)
VALUES
('Fire', 'Controls fire'),
('Ice', 'Controls ice'),
('Earth', 'Controls earth'),
('Air', 'Controls air');