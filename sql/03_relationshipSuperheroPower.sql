DROP TABLE IF EXISTS superhero_power;

CREATE TABLE superhero_power (
    superhero_id int REFERENCES superhero(id) ON DELETE CASCADE,
    power_id int REFERENCES power(power_id) ON DELETE CASCADE,
    CONSTRAINT superhero_power_id PRIMARY KEY (superhero_id, power_id)
);