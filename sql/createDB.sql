DROP TABLE IF EXISTS superhero CASCADE;
CREATE TABLE superhero (
    id serial NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL,
    alias varchar(255) NOT NULL,
    origin varchar(255)
);


DROP TABLE IF EXISTS assistant;
CREATE TABLE assistant (
    id serial NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL
);

ALTER TABLE assistant ADD COLUMN superhero_id int REFERENCES superhero(id);


DROP TABLE IF EXISTS power CASCADE;
CREATE TABLE power (
    power_id serial PRIMARY KEY,
    name varchar(45) NOT NULL,
    description varchar(45) NOT NULL
);

DROP TABLE IF EXISTS superhero_power;
CREATE TABLE superhero_power (
    superhero_id int REFERENCES superhero(id),
    power_id int REFERENCES power(power_id),
    CONSTRAINT superhero_power_id PRIMARY KEY (superhero_id, power_id)
);


INSERT INTO superhero (name, alias, origin) VALUES ('Bruce Wayne', 'Batman', 'Gotham City');
INSERT INTO superhero (name, alias, origin) VALUES ('Clark Kent', 'Superman', 'Krypton');
INSERT INTO superhero (name, alias, origin) VALUES ('Thor Odinson', 'Thor', 'Åsgard');
INSERT INTO superhero (name, alias, origin) VALUES ('Tony Stark', 'Ironman', 'USA');
INSERT INTO superhero (name, alias, origin) VALUES ('Peter Parker', 'Spider-Man', 'USA');

INSERT INTO assistant (name, superhero_id)
VALUES ('Bernt', 1), ('Kjell', 2), ('Geir', 3);

INSERT INTO power
(name, description)
VALUES
('Fire', 'Controls fire'),
('Ice', 'Controls ice'),
('Earth', 'Controls earth'),
('Air', 'Controls air');

INSERT INTO superhero_power (superhero_id, power_id)
VALUES (1, 1), (2, 2), (3, 3), (4, 4), (4, 1);

